from django.urls import path, include
from authentication import views as authenticationViews
from rest_framework import routers
from rest_framework.authtoken import views as tokenViews

app_name = 'authentication'

router = routers.DefaultRouter()
router.register(r'users', authenticationViews.UserViewSet)
router.register(r'groups', authenticationViews.GroupViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('register/', authenticationViews.RegisterAPIView.as_view(), name='register'),
    path('login/', tokenViews.obtain_auth_token, name='login'),
    path('logout/', authenticationViews.LogoutAPIView.as_view(), name='logout')
]
