# HeloPipel

Sebuah messaging app sederhana

# Setup
Proses untuk menjalankan proyek di lokal (sistem operasi Windows)

1.  Clone Repository
    <pre>git clone https://gitlab.com/AnthonyDP/helopipel.git</pre>
<br>

2. Membuat Virtual Environment <br>
Masuk ke dalam folder yang sudah di-clone, kemudian jalankan perintah berikut di command line
    <pre>python -m venv env</pre>
    Kemudian masuk kedalam virtual environment dengan menjalankan script "activate" yang berada pada path berikut
    <pre>env\Scripts\activate</pre>
<br>

3. Install Dependencies<br>
Untuk install dependencies, dapat dilakukan dengan pip install
    <pre>pip install -r requirements.txt</pre>
<br>

4. Setup Database<br>
Proyek ini menggunakan PostgreSQL v14. Silahkan inisiasi database baru menggunakan database management tool yang dipunya
<br>

5. Membuat .env<br>
Buat file dengan nama '.env', dan masukkan text ini kedalam file
    <pre>
    SECRET_KEY=%insert your secret%
    POSTGRES_NAME=%your database name%
    POSTGRES_USER=%your database user%
    POSTGRES_PASSWORD=%your database user's password%
    POSTGRES_HOST=%your database host%
    POSTGRES_PORT=%your database port%
    GITLAB_ENV=0
    </pre>
<br>

6. Run Application<br>
Perintah awal untuk migrasi data
    <pre>
    python manage.py makemigrations
    python manage.py migrate
    </pre>
    <br>
    Perintah untuk menjalankan aplikasi
    <pre>
    python manage.py runserver
    </pre>

7. Membuat superuser<br>
Dapat dibuat dengan menjalankan perintah berikut
    <pre>
        python manage.py createsuperuser
    </pre>
<br>
8. Alur Aplikasi<br>
    Authentication
    <pre>
    1. Register
    2. Login
    3. Get authToken
    4. Logout
    5. Destroy authToken
    </pre>
<br>
    Messaging <br>
    Semua API messaging memerlukan authToken di header authorization
    <pre>
    1. /messaging/user/list/
        melihat semua user yang terdaftar
    </pre>
    <pre>
    2. /messaging/topic/list/
        melihat semua topik/percakapan yang sudah dimiliki
    </pre>
    <pre>
    3. /messaging/topic/create/
        membuat sebuah topik/percapakan dengan user dipilih, jika sebelumnya belum memiliki topik/percakapan dengan user dipilih
    </pre>
    <pre>
    4. /messaging/topic/detail?topic_id=
        melihat semua pesan dalam sebuah topik/percakapan milik sendiri, sekaligus "read" semua pesan dari user yang lain dalam satu topik/percakapan
    </pre>
    <pre>
    5. /messaging/message/send/
        mengirimkan pesan kepada user yang lain dalam satu topik/percakapan, hanya bisa mengirim pesan ketika sudah ada topik/percakapan antara dua user
    </pre>
<br>
9. Collection API
    collection API bisa didapatkan dari file "HeloPipel.postman_collection.json"