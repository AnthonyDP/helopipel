# Generated by Django 4.0 on 2021-12-18 01:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('messaging', '0003_alter_topic_last_updated_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='sender_user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='message_user', to='auth.user'),
        ),
        migrations.AlterField(
            model_name='message',
            name='topic',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='message_topic', to='messaging.topic'),
        ),
        migrations.AlterField(
            model_name='topic',
            name='user_one',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='topic_user_one', to='auth.user'),
        ),
        migrations.AlterField(
            model_name='topic',
            name='user_two',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='topic_user_two', to='auth.user'),
        ),
    ]
