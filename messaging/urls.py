from django.urls import path, include
from rest_framework import routers
from messaging import views as messagingViews


app_name = 'messaging'

router = routers.DefaultRouter()
router.register(r'topics', messagingViews.TopicViewSet)
router.register(r'messages', messagingViews.MessageViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('user/list/', messagingViews.UserListAPIView.as_view(), name='user/list'),
    path('topic/list/', messagingViews.TopicListAPIView.as_view(), name='topic/list'),
    path('topic/create/', messagingViews.TopicCreateAPIView.as_view(), name='topic/create'),
    path('topic/detail/', messagingViews.TopicDetailAPIView.as_view(), name='topic/detail'),
    path('message/send/', messagingViews.MessageSendAPIView.as_view(), name='message/send')
]