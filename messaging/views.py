from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework import generics, serializers, status, viewsets
from rest_framework import permissions, authentication
from rest_framework.response import Response
from messaging.models import Message, Topic
from messaging.serializers import MessageSerializer, SendMessageSerializer, TopicCreateSerializer, TopicListSerializer, TopicSerializer, UserListSerializer

# Create your views here.

class TopicViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Topic.objects.all().order_by('-last_updated_date')
    serializer_class = TopicSerializer
    permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]


class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Message.objects.all().order_by('-send_date')
    serializer_class = MessageSerializer
    permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]

class UserListAPIView(generics.GenericAPIView):

    serializer_class = UserListSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get(self, request):
        user = request.user

        #exclude any admin
        queryset = User.objects.exclude(id=user.id).exclude(is_staff=True).exclude(is_superuser=True)

        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class TopicListAPIView(generics.GenericAPIView):

    serializer_class = TopicListSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get(self, request):
        user = request.user

        #exclude any admin
        queryset = Topic.objects.raw(
            "SELECT mt.id, mt.last_message, mt.last_updated_date, "\
            + "CASE WHEN user_one_id != " + str(user.id) + " THEN " + str(user.id)\
            + " ELSE user_one_id END AS user_one_id, "\
            + "CASE WHEN user_two_id = " + str(user.id) + " THEN user_one_id"\
            + " ELSE user_two_id END AS user_two_id "\
            + "FROM messaging_topic mt WHERE user_one_id = "\
            + str(user.id) + " OR user_two_id = " + str(user.id) + "")

        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class TopicCreateAPIView(generics.GenericAPIView):
    
    serializer_class = TopicCreateSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def post(self, request):
        user = request.user
        data = request.data

        #filter is user exist
        queryset = User.objects.filter(id=data['receiver_user_id'])
        if not (queryset.exists()):
            return Response({"error": "USR_NOT_EXIST"},status=status.HTTP_406_NOT_ACCEPTABLE)

        #filter is topic exist
        query1 = Q(user_one_id=user.id)
        query1.add(Q(user_one_id=data['receiver_user_id']), Q.OR)
        query2 = Q(user_two_id=user.id)
        query2.add(Q(user_two_id=data['receiver_user_id']), Q.OR)
        query1.add(query2, Q.AND)
    
        queryset = Topic.objects.filter(query1)
        if (queryset.exists()):
            return Response({"error": "TPC_EXIST"},status=status.HTTP_406_NOT_ACCEPTABLE)
    
        serializer_data = {
            "last_message": "",
            "user_one": user.id,
            "user_two": data['receiver_user_id']
            }
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        posted_data = serializer.data
        return Response(posted_data, status=status.HTTP_201_CREATED)

class TopicDetailAPIView(generics.GenericAPIView):
    
    queryset = Topic.objects.all().order_by('-last_updated_date')
    serializer_class = TopicSerializer
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]

    def get(self, request):
        user = request.user
        data = request.GET

        if (data['topic_id']==None or data['topic_id']==''):
            return Response({"error": "BAD_REQUEST"},status=status.HTTP_400_BAD_REQUEST)
        
        # filter is topic exist
        queryset = Topic.objects.filter(id=data['topic_id'])
        if not (queryset.exists()):
            return Response({"error": "TPC_NOT_EXIST"},status=status.HTTP_406_NOT_ACCEPTABLE)
        
        #filter is it user topic or not
        query1 = Q(user_one_id=user.id)
        query1.add(Q(user_two_id=user.id), Q.OR)

        queryset = queryset.filter(query1).order_by('-last_updated_date')
        if not (queryset.exists()):
            return Response({"error": "TPC_NOT_EXIST"},status=status.HTTP_406_NOT_ACCEPTABLE)

        #set another person message to read
        messageQuery = Message.objects.filter(topic__in=queryset).order_by('-send_date')
    
        for message in messageQuery:
            if (message.sender_user_id != user.id):
                message.is_read_by_receiver = True
                message.save()

        #serialize data
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

class MessageSendAPIView(generics.GenericAPIView):
    
        serializer_class = SendMessageSerializer
        permission_classes = [permissions.IsAuthenticated]
        authentication_classes = [authentication.TokenAuthentication]

        def post(self, request):
            user = request.user
            data = request.data

            #filter is user exist
            queryset = User.objects.filter(id=data['receiver_user_id'])
            if not (queryset.exists()):
                return Response({"error": "USR_NOT_EXIST"},status=status.HTTP_406_NOT_ACCEPTABLE)

            #filter is topic exist
            query1 = Q(user_one_id=user.id)
            query1.add(Q(user_one_id=data['receiver_user_id']), Q.OR)
            query2 = Q(user_two_id=user.id)
            query2.add(Q(user_two_id=data['receiver_user_id']), Q.OR)
            query1.add(query2, Q.AND)
        
            queryset = Topic.objects.filter(query1)
            queryset = queryset.filter(id=data['topic_id'])
            if not (queryset.exists()):
                return Response({"error": "TPC_NOT_EXIST"},status=status.HTTP_406_NOT_ACCEPTABLE)
            
            serializer_data = {
                "topic": data['topic_id'],
                "message": data['message'],
                "sender_user": user.id,
                }
            
            for topic in queryset:
                topic.last_message = data['message']
                topic.save()

            serializer = self.serializer_class(data=serializer_data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            posted_data = serializer.data
            return Response(posted_data, status=status.HTTP_201_CREATED)