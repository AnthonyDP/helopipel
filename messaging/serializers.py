from django.contrib.auth.models import User
from rest_framework import serializers
from messaging.models import Message, Topic

class MessageSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Message
        fields = '__all__'

class TopicSerializer(serializers.ModelSerializer):
    message_topic = MessageSerializer(read_only=True, many=True)

    class Meta:
        model = Topic
        fields = '__all__'

class UserListSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']

class TopicListSerializer(serializers.ModelSerializer):
    user_one = UserListSerializer(read_only=True, many=False)
    user_two = UserListSerializer(read_only=True, many=False)

    class Meta:
        model = Topic
        fields = ['id', 'user_one', 'user_two', 'last_message', 'last_updated_date']

class TopicCreateSerializer(serializers.ModelSerializer):
    user_one = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    user_two = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        model = Topic
        fields = ['user_one', 'user_two']

class SendMessageSerializer(serializers.ModelSerializer):
    topic = serializers.PrimaryKeyRelatedField(queryset=Topic.objects.all())
    sender_user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    
    class Meta:
        model = Message
        fields = ['topic', 'sender_user', 'message']