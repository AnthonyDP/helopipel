from django.contrib import admin
from messaging import models

# Register your models here.
admin.site.register(models.Topic)
admin.site.register(models.Message)