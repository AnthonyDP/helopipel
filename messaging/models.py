from django.conf import settings
from django.core.checks import messages
from django.db import models
from django.db.models.deletion import CASCADE
from django.utils.timezone import now

# Create your models here.
class Topic(models.Model):
    user_one = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=CASCADE,
        related_name='topic_user_one'
        )
    user_two = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=CASCADE,
        related_name='topic_user_two'
        )
    last_message = models.TextField(blank=True)
    last_updated_date = models.DateTimeField(default=now, blank=True, editable=False)
    created_date = models.DateTimeField(default=now, editable=False)

    def __str__(self) -> str:
        stringRet = "Topic Id: " +  str(self.id)
        return stringRet
    

class Message(models.Model):
    topic = models.ForeignKey(
        Topic,
        on_delete=CASCADE,
        related_name='message_topic'
        )
    sender_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=CASCADE,
        related_name='message_user'
        )
    message = models.TextField()
    send_date = models.DateTimeField(default=now, editable=False)
    is_read_by_receiver = models.BooleanField(default=False, editable=False)

    def __str__(self) -> str:
        stringRet = str(self.sender_user) + "=> Kirim Pesan: " + str(self.message)
        return stringRet
    